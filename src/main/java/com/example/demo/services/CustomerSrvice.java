/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.services;

import com.example.demo.model.Customer;
import java.util.List;


/**
 *
 * @author Hasanka
 */
public interface CustomerSrvice {
    
    public List<Customer> findAll();
    
    public Customer saveCus(Customer customer);
    
    public Customer findById(int id);
    
    public String deleteCus(int id);
    
}
