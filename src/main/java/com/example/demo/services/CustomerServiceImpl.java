/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.services;

import com.example.demo.model.Customer;
import com.example.demo.rpository.CustomerRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hasanka
 */
@Service
public class CustomerServiceImpl implements CustomerSrvice{
    
    @Autowired
    private CustomerRepo customerRepo;
    
    @Override
    public List<Customer> findAll(){
        List<Customer> list = customerRepo.findAll();
        return list;
    }

    @Override
    public Customer saveCus(Customer customer) {
        return customerRepo.save(customer);
    }
    
    @Override
    public Customer findById(int Id){
        return customerRepo.findById(Id).get();
    }

    @Override
    public String deleteCus(int id) {
        customerRepo.deleteById(id);
        return "Deleted";
    }
    
    
    
}
