package com.example.demo.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="customer")
public class Customer implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    private int id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "city")
    private String city;
    
    @Column(name = "contactNum")
    private int contactNum;

    
    public Customer() {
    }

    public Customer(String name, String city, int contactNum) {
        this.name = name;
        this.city = city;
        this.contactNum = contactNum;
    }

    public Customer(int id, String name, String city, int contactNum) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.contactNum = contactNum;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getContactNum() {
        return contactNum;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setContactNum(int contactNum) {
        this.contactNum = contactNum;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name + ", city=" + city + ", contactNum=" + contactNum + '}';
    }
    
    
    
}
