package com.example.demo.controller;

import com.example.demo.model.Customer;
import com.example.demo.services.CustomerSrvice;
import java.awt.PageAttributes;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class customerController {

    @Autowired
    private CustomerSrvice customerSrvice;

    @GetMapping("/data")
    public String userData() {
        return "name:Kasun,Age:28";
    }

    @GetMapping("/")
    public List<Customer> getAllCustomer() {
        return customerSrvice.findAll();
    }
    
    @GetMapping("/cusid/{id}")
    public Customer getCustomerById(@PathVariable(value="id") int id) {
        return customerSrvice.findById(id);
    }

    @RequestMapping(value = "/addCus", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    public Customer process( @RequestBody Customer cus) throws Exception {
        
        Customer saveCus = customerSrvice.saveCus(cus);
        return saveCus;
    }
    
    @RequestMapping(value="/delCus", method = RequestMethod.PUT)
    public String deleteCustomer(@RequestBody String sid) throws Exception{
        System.out.println("\n cusomer Id received : "+ sid);
        
        try{
            int id = Integer.parseInt(sid);
            return customerSrvice.deleteCus(id);
            
        }catch(Exception e){
            throw new Exception("id is not intiger,Unable to delete customer");
        }
        
    }
}
